# Filter::Base

Provides an abstract form object base class to be subclassed for the creation of resource filters.

## Installation

Add the following to your Gemfile:

Using bundler >= 1.7 (recommended, reduces risk of name conflicts with gems hosted at rubygems.org):

    source 'git@gitlab.com:rdavid369/filter-base.git' do
      gem 'filter-base'
    end

Using earlier versions of bundler:

    source 'git@gitlab.com:rdavid369/filter-base.git'
    gem 'filter-base'
    
And then execute:

    $ bundle

## Usage

### General

Filters are typically intended for use in retrieving result sets for a resource in a controller's `index` action.

Subclass Filter::Base and build up your own resource filter class.  Define attributes for the filter in [Virtus](https://github.com/solnic/virtus) style, and implement a `get_scope` (or similar) method to do the filtering. For example:

(Note that the scopes in the example that follows are defined on the `PrescribedBurn` class itself.  They could also be defined via `.where("...")` clauses directly within the `get_scope` method here, but placing them in the `PrescribedBurn` class allows their re-use in other contexts as well.)

```ruby
class PrescribedBurnFilter < Filter::Base
  
  attribute :forestry_area_ids, Array[Integer]
  attribute :date_range_start, Date
  attribute :date_range_end, Date
  attribute :agency_id, Integer
  attribute :sort_field, String
  attribute :sort_order, String
  attribute :authorized_user, AuthorizedUser
  
  def initialize(args={})
    super(args)
  end

  def get_scope
    scope = PrescribedBurn.select(select()).includes(include()).order(order())
    if has_value? :forestry_area_ids
      scope = scope.for_forestry_areas(@forestry_area_ids)
    else
      if has_value? :authorized_user && @authorized_user.is_forestry_area_authority?
        scope = scope.for_forestry_areas(@authorized_user.forestry_area_ids)
      end      
    end    
    if has_value? :date_range_start
      scope = scope.on_or_after(@date_range_start)
    end
    if has_value? :date_range_end
      scope = scope.on_or_before(@date_range_end)
    end
    if has_value? :agency_id
      scope = scope.for_agency(@agency_id)
    end
    return scope
  end
  
  def select
    'forestry_areas.code, prescribed_burns.date, prescribed_burns.identifier, prescribed_burns.planned_acres'
  end
  
  def include
    [:forestry_area]
  end
  
  def order
    'forestry_areas.code, prescribed_burns.date'
  end
end
```

In the controller:

```ruby
class PrescribedBurnsController < ApplicationController
  
  # GET /prescribed_burns
  def index
    # ensure that there is always a filter on the session by initializing with defaults
    # if not yet present
    session[:prescribed_burn_filter] ||= PrescribedBurnFilter.new(
      :date_range_start=>Date.today.beginning_of_year.to_s, 
      :date_range_end=>Date.today.to_s,
      :authorized_user=>current_user
    )
    # use the session filter if there are no incoming filter parameters,
    # otherwise build a new filter from the params
    if params[:prescribed_burn_filter].nil? 
      @prescribed_burn_filter = session[:prescribed_burn_filter] 
    else 
      attrs = params[:prescribed_burn_filter].merge({:authorized_user=>current_user})
      @prescribed_burn_filter = PrescribedBurnFilter.new(attrs)
    end
    # re-assign the session filter
    session[:prescribed_burn_filter] = @prescribed_burn_filter
    # execute the filtering
    scope = @prescribed_burn_filter.get_scope
    # perfom pagination management as necessary
    session[:prescribed_burns_page] ||= 1
    session[:prescribed_burns_page] = params[:page] if params[:page].present?
    @prescribed_burns = scope.page(session[:prescribed_burns_page]).per_page(20)
    # go to the view
    render :action=>:index
  end
```

In the view (`index.html.erb`):

```erb
<%= form_for @prescribed_burn_filter, 
      :url=>{:action=>:index},
      :html=>{:id=>:prescribed_burn_filter_form, :method=>:get} do |f| 
%>

  <table>
    <tr>

      <% attr = :forestry_area_ids %>
      <td>
        <%= PrescribedBurn.human_attribute_name attr %>
      </td>
      <td>
        <%= f.select(attr, forestry_areas_select_options) %>
      </td>
    
      <% attr = :agency_id %>
      <td>
        <%= PrescribedBurn.human_attribute_name attr %>
      </td>
      <td>
        <%= f.select attr, Agency.select_options %>
      </td>
      
    </tr>
    <tr>
      
      <% attr = :date_range_start %>
      <td>
        Start
      </td>
      <td>
        <%= f.text_field attr, :class=>:datepicker, :size=>12 %>
      </td>
    
      <% attr = :date_range_end %>
      <td>
        End
      </td>
      <td>
        <%= f.text_field attr, :class=>:datepicker, :size=>12 %>
      </td>

    </tr>
    
  </table>
  
  <div>
    <%= f.submit 'List' %>
  </div>
  
  <!-- render @prescribed_burns results here -->
      
<% end %>

```

### Filter::Base-provided instance methods

#### `has_value?` <attribute_name>

Used to check that a value is assigned to the specified attribute.  Tests are conditioned
on attribute class name: `Virtus::Attribute`, `Virtus::Attribute::Boolean`, or `Virtus::Attribute::Collection`.  Note
that Collection has only been tested against arrays, and will likely be buggy for hashes.

### Hint

Handling of both singles and collections is stupidly easy with this pattern:

Scopes like the example below don't care if they're passed a single value
or array of values:

```ruby
  scope :for_counties, lambda {|ids| where(:county_id=>ids)}
```

then in the filter model....

```ruby
  attribute :county_ids, Array[Integer]

  def get_scope
    scope = Facility
    if has_value?(@county_ids)
      scope = scope.for_counties(@county_ids)
    end
    return scope
  end
```