# Changelog

## 0.1.1 (2016-06-13)
  `has_value?` method deprecated in favor of `value?` (as is the case for ruby Hash)

## 0.1.0 (2015-05-20)

  `has_value?` method rewritten to cover all attributes, e.g.:  
    `has_value? :forestry_area_ids`  
  or  
    `has_value? :is_funded`  
  or  
    `has_value? :authorized_user`

  previously `has_value?` was specific to arrays, while `boolean?` was used to check booleans.
  Now `has_value?` can be used to check any attribute, array, boolean, or scalar.
  Backward-compatibility is broken with this change.
  
## 0.0.1 

  initial release