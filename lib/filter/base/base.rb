module Filter
  class Base
  
    # Handling of both singles and collections is stupidly easy with this pattern:
    #
    # scopes like the example below don't care if they're passed a single value
    # or array of values:
    #
    #   scope :for_counties, lambda {|ids| where(:county_id=>ids)}
    #
    # then in the filter model...
    #
    #   attribute :county_ids, Array[Integer]
    #
    #   def get_arel
    #     scope = Facility
    #     if has_value?(@county_ids)
    #       scope = scope.for_counties(@county_ids)
    #     end
    #     return scope
    #   end
    #
    
    include Virtus.model
    
    # use ActiveModel so form_for will support @<whatever>_filter
    # - http://railscasts.com/episodes/219-active-model?view=asciicast
    include ActiveModel::Conversion
    include ActiveModel::Validations
    include ActiveModel::Validations::Callbacks
    extend ActiveModel::Naming
    def persisted?
      false
    end
    
    def value?(attribute_name)
      attribute_set = self.class.attribute_set
      attr = attribute_set[attribute_name]
      raise "Attribute not found: #{attribute_name}" if attr.nil? 
      value = send attribute_name
      # choosing what to test against here proves difficult
      # attr.type or attr.type.name seems right, but falls apart at Axiom::Types::Array
      # attr.primitive might work, but might be overly specific
      # going with attr.class.name for now...
      case attr.class.name
      when 'Virtus::Attribute::Boolean'
        # Virtus coerces string to boolean if string is in
        #   %w[ 1 on ON t true T TRUE y yes Y YES 0 off OFF f false F FALSE n no N NO ]
        # otherwise it returns the input string
        # We need this because most of our html form 'blank' options are empty strings,
        # which don't coerce to boolean and remain empty strings
        (value == true || value == false)
      when 'Virtus::Attribute::Collection'
        # TODO: determine effectiveness of Virtus::Attribute::Collection handling
        # works for arrays; would probably work on a set; doubt it would work on a hash
        #
        # used basically as a test against [""] (though obviously more than that is covered), 
        # which is typically what we get inside a filter model when a user doesn't
        # set a value for an item on a filtering page (or selects a blank select option).
        value.any?(&:present?)
      when 'Virtus::Attribute'
        value.present?
      else 
        raise "Unhandled attribute class: #{attr.class.name}"
      end
    end

    alias_method :has_value?, :value?
    
  end
end